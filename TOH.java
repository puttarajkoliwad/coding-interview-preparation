
public class TOH {
	static int count=0;
	public static void tOH(int n, char A, char B, char C) {
		if(n>0) {
			tOH(n-1, A, C, B);
			count++;
			System.out.println(count+" : "+A+" to "+C);
			tOH(n-1, B, A, C);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			System.out.println(1/0);
		}
		/*catch(Exception e) {
			e.getMessage();
		}*///This block will give error because Generic catch block should be the last in catch hierarchy
		catch(ArithmeticException e) {
			System.out.println(e.getMessage());
		}
		finally {
			System.out.println("Exception ignored by finally()");
		}
		tOH(5, 'A', 'B', 'C');
	}
}
